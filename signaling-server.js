/**************/
/*** CONFIG ***/
/**************/
var PORT = 8080;

var existChannels = {};
/*************/
/*** SETUP ***/
/*************/
var express = require('express');
var http = require('https');
var fs = require('fs');
var bodyParser = require('body-parser')
var main = express()
const options = {
    key: fs.readFileSync('./server.key'),
    cert: fs.readFileSync('./server.cert')
  };
var server = http.createServer(options,main)
var io  = require('socket.io').listen(server);

//io.set('log level', 2);

server.listen(PORT,'192.168.68.133', function() {
    console.log("Listening on port " + PORT);
});
//main.use(express.bodyParser());
main.get('/', function(req, res){ res.sendFile(__dirname + '/viewClient.html'); });

main.get('/:id', function(req, res){ res.sendFile(__dirname + '/client.html'); });
main.get('/view/:id', function(req, res){ res.sendFile(__dirname + '/viewClient.html'); });
main.get('/create/:roomName',function(req,res){
    let roomName = req.params.roomName;
    console.log("inside creating room");
    if(roomName in existChannels){
        res.json({success:false,message:"room Already exist"}).status(404);
    }
    else{
        console.log("inside else");
        existChannels[roomName] = true;
        return res.json({success:true, status:500});
        //res.sendFile(__dirname + '/client.html');
    }
})
// main.get('/index.html', function(req, res){ res.sendfile('newclient.html'); });
// main.get('/client.html', function(req, res){ res.sendfile('newclient.html'); });



/*************************/
/*** INTERESTING STUFF ***/
/*************************/
var channels = {};
var sockets = {};

/**
 * Users will connect to the signaling server, after which they'll issue a "join"
 * to join a particular channel. The signaling server keeps track of all sockets
 * who are in a channel, and on join will send out 'addPeer' events to each pair
 * of users in a channel. When clients receive the 'addPeer' even they'll begin
 * setting up an RTCPeerConnection with one another. During this process they'll
 * need to relay ICECandidate information to one another, as well as SessionDescription
 * information. After all of that happens, they'll finally be able to complete
 * the peer connection and will be streaming audio/video between eachother.
 */

io.sockets.on('connection', function (socket) {
    socket.channels = {};
    console.log(socket.channels,"-----channels------------");
    sockets[socket.id] = socket;
    // console.log("sockets",Object.keys(sockets),"--------------end here--------------------");
    // console.log("["+ socket.id + "] connection accepted");
    socket.on('disconnect', function () {
        for (var channel in socket.channels) {
            console.log(channel,"part -----end here----");
            part(channel);
        }
        console.log("["+ socket.id + "] disconnected");
        delete sockets[socket.id];
    });


    socket.on('join', function (config) {
        console.log("["+ socket.id + "] join ", config);
        var channel = config.channel;
        var userdata = config.userdata;
        console.log("socket.channels",socket.channels,channels);
        if (channel in socket.channels) {
            console.log("["+ socket.id + "] ERROR: already joined ", channel);
            return;
        }

        if (!(channel in channels)) {
            console.log(channels);
            channels[channel] = {};
        }

        for (id in channels[channel]) {
            console.log("id",id);
            channels[channel][id].emit('addPeer', {'peer_id': socket.id, 
            'should_create_offer': false});
            console.log("channels[channel][id]","channels",channels,id);
            socket.emit('addPeer', {'peer_id': id, 'should_create_offer': true});
        }

        channels[channel][socket.id] = socket;
        socket.channels[channel] = channel;
        console.log("socket.channels",socket.channels);
    });

    function part(channel) {
        console.log("["+ socket.id + "] part ");

        if (!(channel in socket.channels)) {
            console.log("["+ socket.id + "] ERROR: not in ", channel);
            return;
        }

        delete socket.channels[channel];
        delete channels[channel][socket.id];

        for (id in channels[channel]) {
            channels[channel][id].emit('removePeer', {'peer_id': socket.id});
            socket.emit('removePeer', {'peer_id': id});
        }
    }
    socket.on('part', part);

    socket.on('relayICECandidate', function(config) {
        console.log("calling ------------------------------------",socket.id);
        var peer_id = config.peer_id;
        var ice_candidate = config.ice_candidate;
        // console.log("["+ socket.id + "] relaying ICE candidate to [" + peer_id + "] ", ice_candidate);
        // console.log("sockets-------------",sockets[peer_id],"------------",socket.id);
        if (peer_id in sockets) {
            sockets[peer_id].emit('iceCandidate', {'peer_id': socket.id, 'ice_candidate': ice_candidate});
        }
    });

    socket.on('relaySessionDescription', function(config) {
        var peer_id = config.peer_id;
        var session_description = config.session_description;
        // console.log("["+ socket.id + "] relaying session description to [" + peer_id + "] ", session_description);

        if (peer_id in sockets) {
            sockets[peer_id].emit('sessionDescription', {'peer_id': socket.id, 'session_description': session_description});
        }
    });
});
